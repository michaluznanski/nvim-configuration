-- Konfiguracja szukajki
vim.opt.incsearch  = true
vim.opt.hlsearch   = true
vim.opt.ignorecase = true
vim.opt.smartcase  = true

vim.opt.wrapscan   = false

-- Autorozpoznawanie typów plików
vim.cmd("filetype plugin indent on")
-- vim.cmd("filetype indent off")
vim.opt.autoindent = false
vim.opt.smartindent = true

-- Zawijanie długich linii
vim.opt.wrap = false

-- Wyświetlanie białych znaków
-- set list
-- spacje na końcu można odróżnić ustawiając "trail"
vim.opt.listchars = { tab = '>-', space = '_' }

-- Ustawienia tabulatur
Indent_settings = function(taby_czy_spacje)
    assert(taby_czy_spacje == "taby" or taby_czy_spacje == "spacje", 'Ma być "taby" albo "spacje"')
    if taby_czy_spacje == "taby" then
        -- Z wykorzystaniem tabów:
        vim.opt.tabstop = 4
        vim.opt.shiftwidth = 0    -- Ile spacji przesunąć poprzez < (0 -> =tabstop)
        vim.opt.expandtab = false -- Czy przekształcać taby w w spacje
        vim.opt.softtabstop = 0   -- Ile spacji wstawić za taba (0 -> wyłącza sts)
    elseif taby_czy_spacje == "spacje" then
        -- Bez tabów
        vim.opt.tabstop = 8     -- Domyślna wartość tabstopa
        vim.opt.softtabstop = 0 -- Domyślna wartość, wyłącza sts
        vim.opt.shiftwidth = 4  -- Ile spacji na stopień indentacji (0 -> =tabstop)
        vim.opt.smarttab = true -- Użycie taba w pustej linii wstawia shiftwidth spacji,
        -- jeśli expand tab jest włączone, jeśli nie to w zależności
        -- od sts, jeśli włączone, jeśli nie to od ts
        vim.opt.expandtab = true -- Przekształcaj taby w shiftwidth spacji
    end
end
Indent_settings("spacje") -- Domyślne ustawienie

-- Poprzeczka
-- vim.opt.colorcolumn = "80"

-- Konfiguracja popupu autouzupełniania
vim.opt.completeopt = { 'menuone', 'preview', 'noinsert', 'noselect' }

-- Konfiguracja indentacji po słowach kluczowych
-- vim.opt.cinwords={ 'if','else','while','do','for','switch' } -- Domyślne wartości

-- Konfiguracja Lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    -- zainstaluj lazy
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable",
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

local plugins_dict = {
    {
        "ZhiyuanLck/smart-pairs",
        ft = { "py", "cpp", "c", "lua", "bash", "sh" },
        lazy = false,
        config = function()
            require("pairs"):setup()
        end
    },
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        lazy = false
    },
    {
        "stevearc/aerial.nvim",
        lazy = false,
        config = function()
            require("aerial").setup({
                filter_kind = false
            })
            vim.keymap.set("n", "<space>aa", "<cmd>AerialToggle!<CR>")
            vim.keymap.set("n", "<space>ap", "<cmd>AerialPrev<CR>", { buffer = bufnr })
            vim.keymap.set("n", "<space>an", "<cmd>AerialNext<CR>", { buffer = bufnr })
        end
    },


    -- Zarządzanie projektami
    {
        'natecraddock/sessions.nvim',
        lazy = false,
        config = function()
            require('sessions').setup({
                session_filepath = ".nvim/session",
            })
        end
    },
    {
        'natecraddock/workspaces.nvim',
        config = function()
            require("workspaces").setup({
                hooks = {
                    -- TODO Podczas przełączania próbować zamknąć wszystkie dotychczasowe
                    -- bufory oraz (opcjonalnie) skasować lokalne opcje.
                    -- Jeśli są niezapisane zmiany, to albo krzyczeć i kazać ręcznie zapisać,
                    -- albo jak w codzie tworzyć tymczasowe kopie zapasowe.
                    -- Podczas tworzenia nowej sesji utwórz plik sesji
                    add = function()
                        require("sessions").save(nil, { silent = false })
                    end,
                    -- Jeśli sesja istnieje, zapisz przed przełączeniem
                    open_pre = function()
                        if vim.fn.filereadable('.nvim/session') ~= 0 then
                            require("sessions").save(nil, { silent = false })
                        end
                    end,
                    -- Po przełączaniu workspace'a automatycznie załaduj sesję
                    open = function()
                        require("sessions").load(nil, {
                            autosave = true,
                            silent = false
                        })
                        require('config-local').source()
                    end,
                }
            })
        end,
        lazy = false
    },
    {
        'klen/nvim-config-local',
        config = function()
            require('config-local').setup {
                -- Default options (optional)

                -- Config file patterns to load (lua supported)
                config_files = { ".nvim/config.lua" },

                -- Where the plugin keeps files data
                hashfile = vim.fn.stdpath("data") .. "/config-local",

                autocommands_create = true, -- Create autocommands (VimEnter, DirectoryChanged)
                commands_create = true,      -- Create commands (ConfigLocalSource, ConfigLocalEdit, ConfigLocalTrust, ConfigLocalIgnore)
                silent = false,              -- Disable plugin messages (Config loaded/ignored)
                lookup_parents = false,      -- Lookup config files in parent directories
            }
        end,
        lazy = false
    },
    {
        "kyazdani42/nvim-tree.lua",
        dependencies = {
            "nvim-tree/nvim-web-devicons"
        },
        config = function()
            require("nvim-tree").setup()
            vim.keymap.set('n', '<space>ntt', '<cmd>NvimTreeToggle<CR>', opts)
            vim.keymap.set('n', '<space>ntf', '<cmd>NvimTreeFindFile<CR>', opts)
        end,
        lazy = false
    },

    -- Debug
    { 'mfussenegger/nvim-dap',          lazy = false },
    {
        'mfussenegger/nvim-dap-python',
        lazy = false,
        config = function()
            require("dap-python").setup()
            require("dap-python").test_runner = "pytest"
        end
    },
    {
        "rcarriga/nvim-dap-ui",
        dependencies = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio" },
        config = function ()
            require("dapui").setup({
                floating = {
                    border = "rounded",
                    mappings = {
                        close = {"<Esc>"}
                    }
                }
            })
        end,
        lazy = false
    },
    { 'theHamsta/nvim-dap-virtual-text' },
    -- {'jedrzejboczar/nvim-dap-cortex-debug'}, -- TODO skonfigurować

    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        lazy = false,
        config = function()
            require('nvim-treesitter.configs').setup({
                highlight = {
                    enable = true,
                    additional_vim_regex_highlighting = false,
                },
                incremental_selection = {
                    enable = false,
                }
            })
        end
    },

    -- LSP
    {
        'neovim/nvim-lspconfig',
        lazy = false
    },
    { 'hrsh7th/nvim-cmp' },
    { 'hrsh7th/cmp-nvim-lsp' },
    { 'saadparwaiz1/cmp_luasnip' },
    { 'L3MON4D3/LuaSnip' },
    {
        'https://github.com/ray-x/lsp_signature.nvim',
        event = 'VeryLazy',
    },

    { "folke/neodev.nvim",              opts = {} },
    { 'p00f/clangd_extensions.nvim',    lazy = false },
    { "jose-elias-alvarez/null-ls.nvim" },
    {
        "mfussenegger/nvim-lint",
        lazy = false,
        config = function()
            local lint = require("lint")
            lint.linters_by_ft = {
                python = { "pylint" }
            }
            vim.api.nvim_create_autocmd({ "BufWritePost" }, {
                callback = function()
                    lint.try_lint()
                end
            })
        end
    }
}

local opts_dict = {
    install = {
        missing = true,
        colorscheme = { "default" }
    },
    defaults = {
        lazy = true
    }
}

require("lazy").setup(plugins_dict, opts_dict)


-- Skróty
--
-- Telescope
local telescope = require('telescope')
local telbuilt = require('telescope.builtin')
vim.keymap.set('n', '<space>tf', telbuilt.find_files, {})
vim.keymap.set('n', '<space>txf', function() telbuilt.find_files({ no_ignore = true, hidden = true }) end, {})
vim.keymap.set('n', '<space>tb', telbuilt.buffers, {})
vim.keymap.set('n', '<space>tc', telbuilt.commands, {})
vim.keymap.set('n', '<space>tk', telbuilt.keymaps, {})
vim.keymap.set('n', '<space>tq', telbuilt.quickfix, {})
vim.keymap.set('n', '<space>tl', telbuilt.loclist, {})
vim.keymap.set('n', '<space>tgb', telbuilt.current_buffer_fuzzy_find, {}) -- grep buff
vim.keymap.set('n', '<space>tgg', telbuilt.live_grep, {})                 -- grep workspace
vim.keymap.set('n', '<space>tgs', telbuilt.grep_string, {})               -- grep string in cwd
vim.keymap.set('n', '<space>txgg',
    function() telbuilt.live_grep({ additional_args = { "--hidden", "--no-ignore-vcs" } }) end,
    {})
telescope.load_extension("workspaces")
vim.keymap.set('n', '<space>tw', telescope.extensions.workspaces.workspaces, {})
require("telescope").load_extension("aerial")
vim.keymap.set('n', '<space>ta', telescope.extensions.aerial.aerial, {})

vim.keymap.set('n', '<space>tgci', telbuilt.lsp_incoming_calls, {})
vim.keymap.set('n', '<space>tgco', telbuilt.lsp_outgoing_calls, {})

-- Inicjalizacja LSP
require('lsp-config-nvim-cmp') -- skrypt w folderze lua

-- Dodatkowe skróty
-- TODO koment vim.keymap.set({'n', 'i'}, '<c-/>', telbuilt.find_files, {})
-- TODO plugin do templatów

-- Ustaw sygnalizacje terminal-mode'a
vim.api.nvim_create_autocmd("TermEnter", {
    pattern = "*",
    callback = function()
        vim.opt.cursorline = true
    end
})
vim.api.nvim_create_autocmd("TermLeave", {
    pattern = "*",
    callback = function()
        vim.opt.cursorline = false
    end
})


-- Ustaw autoformat pythonowych plików przy użyciu blacka
local enable_py_autoformat = false
Format_python_buffer = function()
    vim.cmd("%!black - -q")
end
vim.keymap.set('n', '<space>pft', function()
    vim.print("Toggling python autoformat on save to " .. (not enable_py_autoformat and "true" or "false"))
    enable_py_autoformat = not enable_py_autoformat
end)
vim.keymap.set('n', '<space>pff', function()
    Format_python_buffer()
end)
vim.api.nvim_create_autocmd("BufWritePre", {
    pattern = "*.py",
    callback = function()
        if enable_py_autoformat then
            Format_python_buffer()
        end
    end
})
