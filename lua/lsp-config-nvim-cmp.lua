-- Na początku dodaj jeszcze wsparcie dla vima w lui:
local neodev = require('neodev')
neodev.setup({}) -- skonfiguruj vimowe podpowiedzi dla luy

-- Add additional capabilities supported by nvim-cmp
local capabilities = require("cmp_nvim_lsp").default_capabilities()

local lspconfig = require('lspconfig')

-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
local servers = { 'pyright', 'lua_ls', 'zls', 'rust_analyzer' }
for _, lsp in ipairs(servers) do
    lspconfig[lsp].setup {
        capabilities = capabilities,
        on_attach = function()
            if vim.lsp.inlay_hint then
                vim.lsp.inlay_hint.enable(true)
            end
        end,
    }
end
-- Dla clangda nieco inaczej
lspconfig.clangd.setup({
    cmd = { 'clangd', '-j=8', '--clang-tidy', '-header-insertion=never',
        '--query-driver=/usr/bin/arm-none-eabi-g*,/usr/bin/clang*,/opt/zephyr-sdk/**', '--enable-config',
    },
    on_attach = function()
        -- require("clangd_extensions.inlay_hints").setup_autocmd() require("clangd_extensions.inlay_hints").set_inlay_hints()
        vim.lsp.inlay_hint.enable(true)
    end,
    capabilities = capabilities,
    filetypes = { "c", "cpp" },
})

-- Ignoruj raka
for _, method in ipairs({ 'textDocument/diagnostic', 'workspace/diagnostic' }) do
    local default_diagnostic_handler = vim.lsp.handlers[method]
    vim.lsp.handlers[method] = function(err, result, context, config)
        if err ~= nil and err.code == -32802 then
            return
        end
        return default_diagnostic_handler(err, result, context, config)
    end
end

-- lsp_signature setup
require('lsp_signature').setup()

-- luasnip setup
local luasnip = require 'luasnip'

-- nvim-cmp setup
local cmp = require 'cmp'
local cmp_confirm = {
    i = function(fallback)
        if cmp.visible() and cmp.get_selected_entry() then
            cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = true })
        else
            fallback()
        end
    end,
    s = cmp.mapping.confirm({ select = true }),
    -- c = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = true }),
}
cmp.setup {
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },
    mapping = cmp.mapping.preset.insert({
        ['<C-u>'] = cmp.mapping.scroll_docs(-4), -- Up
        ['<C-d>'] = cmp.mapping.scroll_docs(4),  -- Down
        ['<C-Space>'] = cmp.mapping.complete(),

        ["<C-j>"] = cmp.mapping(cmp_confirm),
        ["<CR>"] = cmp.mapping(cmp_confirm),

        -- TODO zbyt irytujace, trzeba czegos innego["<ESC>"] = cmp.mapping.abort(),
        -- ["<ESC>"] = cmp.mapping.abort(),
        ['<Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item({
                    behavior = cmp.SelectBehavior.Select
                })
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            else
                fallback()
            end
        end, { 'i', 's' }),
        ['<S-Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item({
                    behavior = cmp.SelectBehavior.Select
                })
            elseif luasnip.jumpable(-1) then
                luasnip.jump(-1)
            else
                fallback()
            end
        end, { 'i', 's' }),
    }),
    completion = {
        -- autocomplete = false, -- ustawiajac na false wyłącza się samowyskakiwanie
        completeopt = "menuone,preview,noinsert"
    },
    sources = {
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
    },
    experimental = {
        ghost_text = true
    }
}
-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<space>do', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<space>dl', vim.diagnostic.setloclist)
vim.keymap.set('n', '<space>dq', vim.diagnostic.setqflist)
vim.keymap.set('n', '<space>de', vim.diagnostic.enable)
vim.keymap.set('n', '<space>dd', function() vim.diagnostic.enable(false) end)

-- Debug
local dap = require('dap')
local dapui = require('dapui')
dap.adapters.rust_gdb = {
    type = "executable",
    command = "rust-gdb",
    args = {
        "--interpreter=dap",
        "--eval-command", "set print pretty on"
    }
}
dap.configurations.rust = {
    {
        name = "Launch",
        type = "rust_gdb",
        request = "launch",
        program = function()
            return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. '/', 'file')
        end,
        cwd = "${workspaceFolder}",
        stopAtBeginningOfMainSubprogram = true,
    },
}

-- Debug keys
vim.keymap.set('n', '<space>xbt', dap.toggle_breakpoint)
vim.keymap.set('n', '<F12>', dap.toggle_breakpoint)
vim.keymap.set('n', '<space>xbl', dap.list_breakpoints)
vim.keymap.set('n', '<space>xbc', dap.clear_breakpoints)

vim.keymap.set('n', '<space>xs', dap.step_into)
vim.keymap.set('n', '<F2>', dap.step_into)
vim.keymap.set('n', '<space>xn', dap.step_over)
vim.keymap.set('n', '<F3>', dap.step_over)
vim.keymap.set('n', '<space>xf', dap.step_out)
vim.keymap.set('n', '<F4>', dap.step_out)

vim.keymap.set('n', '<space>xgu', dap.up)
vim.keymap.set('n', '<F9>', dap.up)
vim.keymap.set('n', '<space>xgd', dap.down)
vim.keymap.set('n', '<F10>', dap.down)

vim.keymap.set('n', '<space>xc', dap.continue)
vim.keymap.set('n', '<F5>', dap.continue)
vim.keymap.set('n', '<space>xp', dap.pause)
vim.keymap.set('n', '<F6>', function()
    for id, _ in pairs(dap.session().threads) do
        dap.pause(id)
    end
end)
vim.keymap.set('n', '<space>xr', dap.restart)
vim.keymap.set('n', '<F7>', dap.restart)
vim.keymap.set('n', '<space>xt', dap.terminate)
vim.keymap.set('n', '<F8>', dap.terminate)

vim.keymap.set('n', '<space>xr', dap.repl.toggle)
vim.keymap.set('n', '<space>xe', function() vim.cmd('DapEval') end)

vim.keymap.set('n', '<space>xuu', dapui.toggle)
vim.keymap.set('n', '<space>xub', function() dapui.float_element('breakpoints') end)
vim.keymap.set('n', '<space>xus', function() dapui.float_element('stacks') end)
vim.keymap.set('n', '<space>xuh', function() dapui.float_element('hover') end)
vim.keymap.set('n', '<space>xuc', function() dapui.float_element('console') end)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
    group = vim.api.nvim_create_augroup('UserLspConfig', {}),
    callback = function(ev)
        -- Buffer local mappings.
        -- See `:help vim.lsp.*` for documentation on any of the below functions
        local opts = { buffer = ev.buf }
        vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
        vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
        vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
        vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
        vim.keymap.set('n', 'gci', vim.lsp.buf.incoming_calls, opts)
        vim.keymap.set('n', 'gco', vim.lsp.buf.outgoing_calls, opts)

        vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
        vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)

        vim.keymap.set('n', 'crn', vim.lsp.buf.rename, opts)
        vim.keymap.set({ 'n', 'v' }, 'cra', vim.lsp.buf.code_action, opts)

        vim.keymap.set('n', '<space>ltd', vim.lsp.buf.type_definition, opts)
        vim.keymap.set('n', '<space>lf', function()
            if vim.bo.filetype == "python" then
                Format_python_buffer()
            else
                vim.lsp.buf.format()
            end
        end, opts)

        vim.keymap.set('n', '<space>lh', function()
            vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
        end, opts)

        vim.keymap.set('n', '<space>lwa', vim.lsp.buf.add_workspace_folder, opts)
        vim.keymap.set('n', '<space>lwr', vim.lsp.buf.remove_workspace_folder, opts)
        vim.keymap.set('n', '<space>lwl', function()
            print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        end, opts)
    end,
})

require("clangd_extensions").setup({})
vim.keymap.set('n', '<space>Cth', function()
    vim.cmd('ClangdTypeHierarchy')
end, {})
vim.keymap.set('n', '<space>Ca', function()
    vim.cmd('ClangdAST')
end, {})
vim.keymap.set('v', '<space>Ca', function()
    vim.cmd("'<,'>ClangdAST")
end, {})
vim.keymap.set('n', '<space>Cm', function()
    vim.cmd('ClangdMemoryUsage')
end, {})
vim.keymap.set('n', '<space>Cs', function()
    vim.cmd('ClangdSymbolInfo')
end, {})
vim.keymap.set('n', '<space>Ch', function()
    vim.cmd('ClangdSwitchSourceHeader')
end, {})
